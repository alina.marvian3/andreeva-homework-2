import baseConfig from './playwright.config';

const configChromium = {
  ...baseConfig,
  projects: [
    {
      name: 'chromium',
      use: {
        browserName: 'chromium',
      },
    },
  ],
};

export default configChromium;
