import { test, expect } from '@playwright/test';
import { successResponse } from '../__mocks__/success-response';

test.skip('При успешном ответе API отображается ссылка с именем кота', async ({
  page,
}) => {
  await page.route(
    request => request.href.includes('/cats/allByLetter'),
    async route => {
      await route.fulfill({
        json: successResponse,
      });
    }
  );

  await page.goto(
    '/iframe.html?args=&id=catslist--get-cats-list&viewMode=story'
  );
  await expect(page.getByText('Аааа')).toBeVisible();
});
