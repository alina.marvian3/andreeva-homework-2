import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture } from '../__page-object__';

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

const isSorted = (arr: number[]): boolean =>
  arr.every((value, index, array) => index === 0 || array[index - 1] >= value);

test('При ошибке сервера в методе rating - отображается попап ошибки', async ({
  page,
  ratingPage,
}) => {
  await ratingPage.mockRatingAPI();

  await ratingPage.openRatingPage();
  await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
});

test('Рейтинг котиков отображается по убыванию', async ({
  page,
  ratingPage,
}) => {
  await ratingPage.openRatingPage();
  await page.waitForSelector(ratingPage.likeRatingItems);
  const values = await ratingPage.getElements();
  expect(isSorted(values)).toBeTruthy();
});
