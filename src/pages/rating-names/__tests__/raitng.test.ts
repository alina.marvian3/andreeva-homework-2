import { test, expect } from '@playwright/test';
import { serverError } from '../__mocks__/mockServerResponse';

test('При ошибке сервера в методе rating - отображается попап ошибки', async ({
  page,
}) => {
  await page.route(
    request => request.href.includes('/api/likes/cats/rating'),
    async route => {
      await route.fulfill({
        contentType: 'application/json',
        ...serverError,
      });
    }
  );

  await page.goto('/rating');
  await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
});

test('Рейтинг котиков отображается в порядке убывания', async ({ page }) => {
  await page.goto('/rating');
  const likesElements = page.locator(
    'td.rating-names_item-count__1LGDH.has-text-success'
  );
  const likesCount = await likesElements.count();

  const likesArray = [];

  for (let i = 0; i < likesCount; i++) {
    const likeText = await likesElements.nth(i).textContent();
    likesArray.push(parseInt(likeText, 10));
  }

  for (let i = 0; i < likesArray.length - 1; i++) {
    expect(likesArray[i]).toBeGreaterThanOrEqual(likesArray[i + 1]);
  }
});
