import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
import { serverError } from '../__mocks__/mockServerResponse';

export class RatingPage {
    private page: Page;
    public likeRatingItems: string

    constructor({page,}: { page: Page; }) {
        this.page = page;
        this.likeRatingItems = '//td[@class="rating-names_item-count__1LGDH has-text-success"]';
    }


    async openRatingPage() {
        return await test.step('Открываю страницу рейтинга котиков', async () => {
            await this.page.goto('/rating')
        })
    }

    async mockRatingAPI() {
        return await test.step(`Мокирую рейтинг котиков`, async () => {
            await this.page.route(
                request => request.href.includes('/cats/rating'),
                async route => {
                    await route.fulfill(serverError);
                });
        })
    }
    async getElements() {
        const elements = await this.page
            .locator(this.likeRatingItems)
            .allTextContents();
        return elements.map(el => parseInt(el));
    }
}

export type RatingPageFixture = TestFixture<RatingPage, { page: Page; }>;

export const ratingPageFixture: RatingPageFixture = async ({ page }, use) => {
    const ratingPage = new RatingPage({ page });
    await use(ratingPage);
};