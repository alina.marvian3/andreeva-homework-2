export const serverError = {
  status: 500,
  body: JSON.stringify({ message: 'Internal Server Error' }),
};
