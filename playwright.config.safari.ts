import baseConfig from './playwright.config';

const configSafari = {
  ...baseConfig,
  projects: [
    {
      name: 'safari',
      use: {
        browserName: 'webkit',
      },
    },
  ],
};

export default configSafari;